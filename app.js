;(async () => {
	const util = require('util')
	const fs = require('fs')

	const app = require('../spaFramework/framework')
	const to = require('await-to-js').to

	const config = require('./config.json')

	process.on('unhandledRejection', (err, p) => {
		console.log(err)
	})

	const db = await new (require('./model/db.js'))(config.db.name)
		.connect()
		.catch(err => console.error(err))

	const User = require('./model/user.js')(db, config)

	app.setTemplatesFolder('view')
	app.addStaticFolder('static')
	app.setBaseCss(['css/base.css', 'css/formValidation.css'])
	app.setBaseJs([])

	app.isSession = async token => {
		console.log('isSession', token)
		if (Array.isArray(token)) throw new Error('arr')
		//token = Array.isArray(token) ? token[0] : token

		return !util.isNull(await db.findSession(token))
	}

	app.saveSession = async (token, user) => {
		//console.log('saveSession', token, user)
		token = Array.isArray(token) ? token[0] : token

		return !util.isNull(user) ? await db.saveSession(token, user.export()) : null
	}

	app.loadSession = async token => {
		//console.log('loadSession ' + token)
		token = Array.isArray(token) ? token[0] : token

		return new User(await db.findSession(token))
	}

	app.on('menu', async ({user}) => {
		return {isLogged: user.isLogged}
	})

	app.on('notifications', async () => {})
	app.on('index', async ({include}) => include('menu'))
	app.on('disconnect', async ({user}) => {
		await user.disconnect()
		return {redirect: 'index'}
	})

	app.on('register', async ({form, user, include}) => {
		include('menu')

		let err, registering

		if (!util.isNull(form)) {
			;[err, registering] = await to(user.register(form))
			if (err) {
				console.error(err)
				return {notifications: [err.toString()], form}
			}

			return {redirect: 'panel'}
		} else {
			return {form: {}}
		}
	})

	app.on('login', async ({form, user, include}) => {
		include('menu')

		try {
			if (user.isLogged) {
				return {notifications: ['You are already logged'], form: {}}
			} else {
				if (!util.isNull(form)) {
					let err, logging
					;[err, logging] = await to(user.login(form))
					if (err) return {notifications: [err.toString()], form}

					return {redirect: 'panel'}
				} else {
					return {form: {}}
				}
			}
		} catch (err) {
			return {notifications: [err.toString()], form}
			console.error('An error occured during logging, please retry in a moment :' + err)
		}
	})

	app.on('panel', async ({user, include}) => {
		include('menu')
		if (!user.isLogged) return {redirect: 'login'}

		try {
			let err, projects
			;[err, projects] = await to(user.listProjects())
			if (err) return {notifications: [err.toString()], projects: []}

			return {projects}
		} catch (err) {
			return {notifications: [err.toString()]}
			console.error('An error occured during loading panel, please retry in a moment :' + err)
		}
	})

	app.on('create-fiche', async ({form, user, include}) => {
		include('menu')
		if (!user.isLogged) return {redirect: 'login'}

		try {
			if (!util.isNull(form)) {
				let err, projectId
				;[err, projectId] = await to(user.createProject(form))
				if (err) return {notifications: [err.toString()], form}

				return {redirect: 'project?' + projectId}
			} else {
				return {form: {}}
			}
		} catch (err) {
			return {notifications: [err.toString()], form}
			console.error(
				'An error occured during creation of the project, please retry in a moment :' + err
			)
		}
	})

	app.on('project', async ({form, user, include}) => {
		include('menu')
		if (!user.isLogged) return {redirect: 'login'}

		try {
			if (!util.isNull(form)) {
				const [err, project] = await to(db.findProject({_id: Object.keys(form)[0]}))
				if (err) return {notifications: [err.toString()]}

				if (project.userId.toString() === user.data._id.toString()) {
					return {project: project.project, actions: ['edit']}
				} else {
					delete project.project.title
					delete project.project.description

					return {project: project.project, actions: ['accept']}
				}
			} else {
				return {}
			}
		} catch (err) {
			console.error(
				'An error occured during creation of the project, please retry in a moment :' + err
			)
			return {notifications: [err.toString()]}
		}
	})

	app.on('admin', async ({form, user, include}) => {
		include('menu')
		if (!user.isLogged) return {redirect: 'login'}

		if (user.data.role === undefined || user.data.role.find(e => e === 'valid-projects') === -1)
			return {redirect: 'panel'}

		try {
			const [err, projects] = await to(db.findProjects({state: 0}))
			if (err) return {notifications: [err.toString()], needToBeValidate: []}

			return {needToBeValidate: projects}
		} catch (err) {
			return {notifications: [err.toString()], needToBeValidate: []}
			console.error(
				'An error occured during creation of the project, please retry in a moment :' + err
			)
		}
	})

	app.on('validateProject', async ({form, user, include}) => {
		include('menu')
		if (!user.isLogged) return {redirect: 'login'}

		if (user.data.role === undefined || user.data.role.find(e => e === 'valid-projects') === -1)
			return {redirect: 'admin'}

		try {
			if (!util.isNull(form)) {
				const [err, projects] = await to(db.validateProject(form.id))

				if (err) return {redirect: 'admin'}

				return {redirect: 'admin'}
			} else {
				return {redirect: 'admin'}
			}
		} catch (err) {
			return {redirect: 'admin'}
			console.error(
				'An error occured during creation of the project, please retry in a moment :' + err
			)
		}
	})

	app.on('availableProjects', async ({form, user, include}) => {
		include('menu')
		if (!user.isLogged) return {redirect: 'login'}

		try {
			const [err, projects] = await to(db.findProjects({state: 2}))
			if (err) return {notifications: [err.toString()], projects: []}

			return {projects}
		} catch (err) {
			return {notifications: [err.toString()], projects: []}
			console.error(
				'An error occured during creation of the project, please retry in a moment :' + err
			)
		}
	})

	app.listen(3000)
})()

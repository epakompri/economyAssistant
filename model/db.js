const util = require('util')

const bluebird = require('bluebird').Promise
const Sequelize = require('sequelize')
const to = require('await-to-js').to

class Db {
	constructor(dbName, cb) {
		try {
			this.s = new Sequelize({
				host: 'localhost',
				database: dbName,
				username: 'root',
				password: '',
				dialect: 'mysql',
				operatorsAliases: false,

				pool: {
					max: 5,
					min: 0,
					acquire: 30000,
					idle: 10000
				},

				logging: false
			})
		} catch (err) {
			console.error(`error while trying to connect to database : ${err}`)
		}
	}

	async connect() {
		try {
			await this.s.authenticate()

			/*
			
			this.users = bluebird.promisifyAll(db.collection('users'))
			this.projects = bluebird.promisifyAll(db.collection('projects'))
			this.sessions = db.collection('sessions')
			
			*/

			this.users = this.s.define('users', {
				id: {
					type: Sequelize.INTEGER,
					primaryKey: true,
					autoIncrement: true
				},
				pseudo: {type: Sequelize.STRING(16), allowNull: false, unique: true},
				password: {type: Sequelize.STRING, allowNull: false}
			})

			this.sessions = this.s.define('sessions', {
				id: {
					type: Sequelize.INTEGER,
					primaryKey: true,
					autoIncrement: true
				},
				token: {type: Sequelize.STRING(25), allowNull: false, unique: true},
				session: {type: Sequelize.TEXT, allowNull: false}
			})

			await this.s.sync()

			return this
		} catch (e) {
			console.error(e)
		}
	}

	async findUser(user) {
		//console.log('findUser', user)

		const [err, finded] = await to(this.users.findOne({where: user}))
		if (err) throw err

		//console.log(finded)
		if (util.isNull(finded)) {
			return null
		} else {
			return finded.dataValues
		}
	}

	async insertUser(user) {
		console.log('insertUser', user)
		return this.users.create(user)
	}

	async saveSession(token, session) {
		//console.log(token, session)
		return this.sessions.upsert({token, session: JSON.stringify(session)}, {where: {token}})
	}

	async findSession(token) {
		console.log('token', token)
		let [err, session] = await to(this.sessions.findOne({where: {token}}))
		if (err) throw err

		if (util.isNull(session)) {
			return null
		} else {
			session = session.dataValues
			session.session = JSON.parse(session.session)

			console.log('session', session)

			return session.isLogged === false ? null : await this.findUser({id: session.session.id})
		}
	}
}
module.exports = Db

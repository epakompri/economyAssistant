const util = require('util')

const validate = require('form-data-validate')
const to = require('await-to-js').to
const Bluebird = require('bluebird')
const bcrypt = Bluebird.promisifyAll(require('bcrypt'))
const MongoObjectID = require('mongodb').ObjectID

module.exports = (db, config) => {
	class User {
		constructor(data) {
			this.isLogged = !(util.isNull(data) || typeof 'data' === undefined)
			this.data = data || {}
		}

		disconnect() {
			console.log(this)
			this.isLogged = false
			this.data = {}
			console.log(this)
			return true
		}

		async login(args) {
			if (this.isLogged) throw 'You are already logged'
			console.log('args', args)
			const formIsValid = validate({
				notEmpty: [args.pseudo, args.password]
			})

			if (!formIsValid) throw 'The form is invalid'

			let err, userInDb, res
			;[err, userInDb] = await to(db.findUser({pseudo: args.pseudo}))
			if (err) throw 'An error occured during logging, please retry in a moment : ' + err.toString()
			console.log('userInDb', userInDb)
			if (typeof userInDb === 'undefined') throw 'Invalid informations'
			;[err, res] = await to(bcrypt.compareAsync(args.password, userInDb.password))
			if (err) throw 'An error occured during logging, please retry in a moment : ' + err.toString()

			if (!res) throw 'Invalid informations'

			this.isLogged = true
			this.data = userInDb
			return true
		}

		async register(args) {
			if (this.isLogged) throw 'You are already logged'

			const formIsValid = validate({
				notEmpty: [args.pseudo, args.password]
			})

			if (!formIsValid) throw 'The form is invalid'

			let err, hash, insertedUser, logged
			;[err, hash] = await to(bcrypt.hashAsync(args.password, config.bcrypt.rounds))
			if (err)
				throw 'An error occured during registery, please retry in a moment : ' + err.toString()
			const u = Object.assign({}, args)
			u.password = hash
			;[err, insertedUser] = await to(db.insertUser(u))
			if (err) {
				if (err.name === 'SequelizeUniqueConstraintError') {
					throw 'You are already registered'
				} else {
					console.log(err)
					throw 'An error occured during registery, please retry in a moment : ' + err.toString()
				}
			}
			;[err, logged] = await to(this.login(args))
			if (err) throw err

			return logged
		}

		isAdmin() {
			return this.isLogged && this.data.id === 1
		}

		async createProject(form) {
			if (!this.isLogged) throw 'You are not logged'

			let err, result
			;[err, result] = await to(
				db.insertProject({
					userId: this.data._id,
					project: form,
					state: 0
				})
			)
			if (err)
				throw 'An error occured during project creation, please retry in a moment : ' +
					err.toString()

			return result.ops[0]._id
		}

		async listProjects(form) {
			if (!this.isLogged) throw 'You are not logged'

			form = form !== undefined ? form : {}

			Object.assign(form, {userId: new MongoObjectID(this.data._id)})

			const [err, projects] = await to(db.findProjects(form))
			if (err)
				throw 'An error occured during searching for projects, please retry in a moment : ' +
					err.toString()

			return projects || []
		}

		async getProject({id}) {
			const formIsValid = validate({
				notEmpty: [id]
			})

			if (!formIsValid) throw 'The form is invalid'

			const [err, project] = await to(db.findProject({_id: new MongoObjectID(id)}))
			if (err)
				throw 'An error occured during searching for project, please retry in a moment : ' +
					err.toString()

			return project
		}

		export() {
			return this.isLogged ? {isLogged: this.isLogged, id: this.data.id} : {isLogged: this.isLogged}
		}
	}

	return User
}

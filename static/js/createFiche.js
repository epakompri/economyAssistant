;(async () => {
	const form = async ({response, bool, int, url, text, time, date, select, textarea, value}) => {
		text('title', `Titre de la fiche`)
		text('description', `Courte description de la fiche`)
		await response()

		text('economy', 'Économie estimée')
		await response()

		await response(bool('permanentAction', 'Action à effet permanent'))

		const regularEconomy = await response(bool('regularEconomy', 'Économie régulière'))

		if (regularEconomy) {
			int('economyQuantity', '', {value: 1, min: 0})
			select('economyPeriod', 'fois par', ['hour', 'day', 'week', 'month', 'year'])
		} else {
			value('economyQuantity', 1)
			value('economyPeriod', 'life')
		}
		await response()

		const needMaterial = await response(bool('needMaterial', 'Néccéssite du matériel'))

		if (needMaterial) {
			text('neededMaterial', 'Matériel nécessaire (séparé par des virgules)')
			text('neededInvestissement', 'Investissement nécessaire (séparé par des virgules)')
		} else {
			value('neededMaterial', '')
			value('neededInvestissement', '')
		}
		await response()

		textarea('content', 'détailler la manière de réaliser cette économie (rédigez en markdown)')

		await response()
	}

	async function Form(c, cb) {
		let waitings = []

		const setValidClass = el => {
			el.classList.add('valid')
			el.classList.remove('invalid')
		}

		const setInvalidClass = el => {
			el.classList.add('invalid')
			el.classList.remove('valid')
		}

		const resetClass = el => {
			el.classList.remove('invalid')
			el.classList.remove('valid')
		}

		class Interaction {
			constructor(inputs, query = '', id) {
				const q = document.createElement('p')
				q.innerHTML = query
				c.appendChild(q)

				inputs.forEach(input => c.appendChild(input))

				this.delete = () => {
					inputs.forEach(input => input.parentNode.removeChild(input))
					q.parentNode.removeChild(q)
				}
				this.isValid = false
				this.onStateChange = () => {}
				this.value = undefined
				this.id = id
			}
		}

		const TypedInput = (inputType, valid, query, id, defaultValues = {}) => {
			return new Promise(async (resolve, reject) => {
				let input

				if (inputType === 'textarea') {
					input = document.createElement('textarea')
				} else {
					input = document.createElement('input')
					input.setAttribute('type', inputType)
				}
				setInvalidClass(input)

				const r = new Interaction([input], query, id)

				const checkForValid = () => {
					r.isValid = valid(input.value)
					r.onStateChange()
					if (r.isValid) {
						setValidClass(input)
						r.value = input.value
					} else {
						setInvalidClass(input)
					}
				}

				input.addEventListener('input', checkForValid)

				input.addEventListener('keydown', function(event) {
					checkForValid()

					if (event.key === 'Enter' && r.isValid) resolve()
				})

				for (let prop in defaultValues) {
					input[prop] = defaultValues[prop]
				}

				waitings.push(r)
				checkForValid()
			})
		}

		const url = (id, query) =>
			TypedInput(
				'url',
				data =>
					new RegExp(
						'^(https?:\\/\\/)?' + // protocol
						'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name and extension
						'((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
						'(\\:\\d+)?' + // port
						'(\\/[-a-z\\d%@_.~+&:]*)*' + // path
						'(\\?[;&a-z\\d%@_.,~+&:=-]*)?' + // query string
							'(\\#[-a-z\\d_]*)?$', // fragment locator
						'i'
					).test(data),
				query,
				id
			)
		const int = (id, query, defaultValues) =>
			TypedInput('number', data => !isNaN(parseInt(data)), query, id, defaultValues)
		const text = (id, query, defaultValues) =>
			TypedInput('text', data => true, query, id, defaultValues)
		const textarea = (id, query, defaultValues) =>
			TypedInput('textarea', data => true, query, id, defaultValues)
		const time = (id, query) => TypedInput('time', data => !isNaN(Date.parse(data)), query, id)
		const date = (id, query) => TypedInput('date', data => !isNaN(Date.parse(data)), query, id)
		const bool = (id, query, defaultValues) =>
			new Promise(async (resolve, reject) => {
				const accept = document.createElement('button')
				accept.innerHTML = 'Oui'

				const decline = document.createElement('button')
				decline.innerHTML = 'Non'

				const r = new Interaction([accept, decline], query, id)

				decline.onclick = () => {
					r.isValid = true
					r.value = false
					setValidClass(decline)
					resetClass(accept)
					r.onStateChange()
					resolve(false)
				}
				accept.onclick = () => {
					r.isValid = true
					r.value = true
					setValidClass(accept)
					resetClass(decline)
					r.onStateChange()
					resolve(true)
				}

				for (let prop in defaultValues) {
					input[prop] = defaultValues[prop]
				}

				waitings.push(r)
			})
		const select = (id, query, data, defaultValues) =>
			new Promise(async (resolve, reject) => {
				const select = document.createElement('select')
				data.forEach(opt => {
					const option = document.createElement('option')
					option.value = opt
					option.text = opt
					select.add(option)
				})

				const r = new Interaction([select], query, id)

				select.onchange = () => {
					r.isValid = true
					r.value = select.value
					setValidClass(select)
					r.onStateChange()
					resolve(select.value)
				}

				for (let prop in defaultValues) {
					input[prop] = defaultValues[prop]
				}

				waitings.push(r)
				select.onchange()
			})

		const data = []

		const response = arg =>
			new Promise(async (resolve, reject) => {
				if (typeof arg !== 'undefined') {
					if (arg instanceof Promise) {
						const r = await arg
						waitings.forEach(e => {
							e.delete()
							data.push([e.id, e.value])
						})
						waitings = []
						resolve(r)
					} else {
						console.log(arg)
						const r = await Promise.all(arg)
						waitings.forEach(e => {
							e.delete()
							data.push([e.id, e.value])
						})
						waitings = []
						resolve(r)
					}
				} else {
					if (waitings.length > 0) {
						const next = document.createElement('button')
						next.innerHTML = 'Next'
						next.disabled = true
						c.appendChild(next)

						waitings.forEach(e => {
							e.onStateChange = () => {
								if (waitings.reduce((sum, e) => sum && e.isValid, true)) {
									next.disabled = false
									setValidClass(next)
								} else {
									next.disabled = true
									resetClass(next)
								}
							}
						})

						waitings[0].onStateChange()

						next.onclick = () => {
							next.parentNode.removeChild(next)
							let lastValue
							waitings.forEach(e => {
								e.delete()
								data.push([e.id, e.value])
								lastValue = e.value
							})
							waitings = []
							resolve(lastValue)
						}
					} else resolve()
				}
			})

		value = (id, val) => data.push([id, val])

		await cb({response, bool, int, url, text, time, date, select, textarea, value})

		return data.reduce((sum, [id, value]) => {
			sum[id] = value
			return sum
		}, {})
	}

	loadPage('create-fiche', await Form(document.querySelector('long-form'), form))
})()
